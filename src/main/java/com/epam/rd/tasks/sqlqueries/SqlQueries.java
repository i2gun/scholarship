package com.epam.rd.tasks.sqlqueries;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class SqlQueries {
    static String selectWithScholarship = "SELECT st.id, st.name, sc.summ " +
                                            "FROM students AS st " +
                                            "JOIN scholarship AS sc " +
                                            "ON st.id = sc.student_id;";
    static String selectNoScholarship = "SELECT st.id, st.name " +
                                            "FROM students AS st " +
                                            "LEFT JOIN scholarship AS sc " +
                                            "ON st.id = sc.student_id " +
                                            "WHERE sc.student_id IS NULL;";

    static ConnectionSource connectionSource;
    static Connection conn;

    public static void main(String[] args) {
        printQueryResult(selectWithScholarship);

        System.out.println();
        printQueryResult(selectNoScholarship);
    }

    private static void printQueryResult(String query) {
        try {
            connectionSource = ConnectionSource.instance();
            conn = connectionSource.createConnection();
            Statement statement = conn.createStatement();

            ResultSet resultSet = statement.executeQuery(query);

            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            int columnsNumber = resultSetMetaData.getColumnCount();
            StringBuilder firstLine = new StringBuilder();

            if (resultSet.next()) {
                System.out.println("=========================");
                for (int i = 1; i <= columnsNumber; i++) {
                    if (i > 1) {
                        firstLine.append(",  ");
                        System.out.print(",    ");
                    }
                    firstLine.append(resultSet.getString(i));
                    System.out.print(resultSetMetaData.getColumnName(i));
                }
                System.out.println();
                System.out.println("------------------------");

                System.out.println(firstLine);
                while (resultSet.next()) {
                    for (int i = 1; i <= columnsNumber; i++) {
                        if (i > 1) System.out.print(",  ");
                        System.out.print(resultSet.getString(i));
                    }
                    System.out.println();
                }
                System.out.println("=========================");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
